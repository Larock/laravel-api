<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product_order extends Model
{
    protected $fillable = [
        'order_id', 'product_id',
    ];
}
