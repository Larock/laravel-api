<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Product;
use App\product_order;

class OrderController extends Controller
{
    public function getNewOrders(){
        $orders = Order::where('status', 'Request send')->get();
        return response()->json($orders);
    }

    public function getOpenOrders(){
        $orders = Order::where('status', 'In progress')->get();
        return response()->json($orders);
    }

    public function getProductOrdersByOrderId($id){
        $productOrders = product_order::where('order_id', $id)->get();
        return response()->json($productOrders);
    }

    public function getOrderById($id){
        $order = Order::find($id);
        return response()->json($order);
    }

    public function createProductOrder(Request $request){
        $order = Order::find($request->$orderid);
        $ids = explode(",",$request->$productids);
        $order->products()->sync($ids);
    }

    public function updateStatus(Request $request, $id){
        $order = Order::find($id);
        $order->status = 'In progress';
        $order->save();
        return $order;
    }

    public function deleteOrder(Request $request, $id){
        $order = Order::where('id', $id)->delete();
    }
}
