<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement([
            'POT',
            'PAN',
            'KUSSEN',
            'KOFFIESCHOTEL',
            'SPONS',
            'MELKSYSTEEMREINIGER',
            'GROENTEMES',
            'KOM',
            'WIJNGLAS',
            'KOFFIEKOP',
            'LAMP',
            'PEN',
            'BUITENKAARS',
            'LUCIFERS',
            'LIPPENBALSEM',
            'NAGELKNIPPER',
            'KAPSTOK',
            'BODY LOTION',
            'TAFEL',
            'LAMZAC',
            'ARMCHAIR',
            'GREEN EGG',
            'VERBINDINGSHULS',
            'LOOFTLIGHTER',
            'T-SHIRT',
            'NIJNTJE',
            'SPOKENJAGER',
            'SLEUTELHANGER',
            'WINKELWAGENMUNTJE',
            'WENSKAART'
        ]),
        'price' => $faker->numberBetween($min= 1, $max= 150),
        'category' => $faker->randomElement([
            'Koken',
            'Tafelen',
            'Wonen',
            'Bed&Bad',
            'Meubelen',
            'Outdoor',
            'Kids',
            'Workshops',
            'Lifestyle'
        ]),
        'stock' => $faker->numberBetween($min= 0, $max= 25),
        'imageURL' => $faker->randomElement([
            '/img/armchair.jpg',
            '/img/bodylotion.jpg',
            '/img/buitenkaars.jpg',
            '/img/greenegg.jpg',
            '/img/groentemes.jpg',
            '/img/kapstok.jpg',
            '/img/koffiekop.jpg',
            '/img/koffieschotel.jpg',
            '/img/kom.jpg',
            '/img/kussen.jpg',
            '/img/lamp.jpg',
            '/img/lamzac.jpg',
            '/img/lippenbalsem.jpg',
            '/img/looftlighter.jpg',
            '/img/lucifers.jpg',
            '/img/melksysteemreiniger.jpg',
            '/img/nagelknipper.jpg',
            '/img/nijntje.jpg',
            '/img/pan.jpg',
            '/img/pen.jpg',
            '/img/pot.jpg',
            '/img/sleutelhanger.jpg',
            '/img/spokenhanger.jpg',
            '/img/spons.jpg',
            '/img/tafel.jpg',
            '/img/t-shirt.jpg',
            '/img/verbindingshuls.jpg',
            '/img/wenskaart.jpg',
            '/img/wijnglas.jpg',
            '/img/winkelwagenmuntje.jpg'
        ]),
        'brand' => $faker->randomElement([
            'ZUSS',
            'FATBOY',
            'SERAX',
            'DEMEYERE',
            'EASY LIFE',
            'VITRA'
        ])
    ];
});
