<?php

use Illuminate\Database\Seeder;

class Orderseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Order', 30)->create();
    }
}
