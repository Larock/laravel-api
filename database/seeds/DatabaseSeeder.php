<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(ProductsTableSeeder::class);
        //$this->call(UserSeeder::class);
        //$this->call(Orderseeder::class);
        $this->call(ProductOrderSeeder::class);
    }
}
