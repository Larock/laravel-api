<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'products'], function(){
    Route::get("/getAll", "ProductController@getProducts");
    Route::get("/getByCategory/{category}", "ProductController@getProductsByCategory");
    Route::get("/getById/{id}", "ProductController@getProductById");
});

Route::group(['prefix' => 'orders'], function(){
    Route::get("/getNew", "OrderController@getNewOrders");
    Route::get("/getOpen", "OrderController@getOpenOrders");
    Route::get("/getByOrderId/{id}", "OrderController@getProductOrdersByOrderId");
    Route::get("/getOrderById/{id}", "OrderController@getOrderById");
    Route::post("/createProductOrder", "OrderController@createProductOrder");
    Route::put("/updateStatus/{id}", "OrderController@updateStatus");
    Route::Delete("/deleteOrder/{id}", "OrderController@deleteOrder");
});